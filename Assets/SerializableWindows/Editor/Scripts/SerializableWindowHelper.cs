﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Pindwin.SerializableWindows.Editor.Scripts
{
	public static class SerializableWindowHelper
	{
		private static EditorWindow GetExistingEditorWindow(Type type)
		{
			Object[] objectsOfTypeAll = Resources.FindObjectsOfTypeAll(type);
			EditorWindow editorWindow = objectsOfTypeAll.Length <= 0 ? null : (EditorWindow) objectsOfTypeAll[0];
			return editorWindow;
		}

		[DidReloadScripts]
		private static void OnScriptsReload()
		{
			Assembly assembly = typeof(SerializableWindowHelper).Assembly;

			List<Type> interfaceImplementations = assembly.GetTypes()
				.Where(type => typeof(SerializableState).IsAssignableFrom(type) && type.IsClass && !type.IsAbstract)
				.ToList();

			foreach (Type implementation in interfaceImplementations)
			{
				Type genericType = typeof(SerializableWindow<>).MakeGenericType(implementation);

				IEnumerable<Type> windowTypes = assembly.GetTypes()
					.Where(type => genericType.IsAssignableFrom(type));

				foreach (Type windowType in windowTypes)
				{
					EditorWindow editorWindow = GetExistingEditorWindow(windowType);

					if (editorWindow == null)
					{
						continue;
					}

					MethodInfo method = windowType.GetMethod(
						"Initialize",
						BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.NonPublic);

					method?.Invoke(editorWindow, null);
				}
			}
		}
	}
}
