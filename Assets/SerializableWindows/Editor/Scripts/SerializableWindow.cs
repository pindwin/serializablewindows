﻿using Pindwin.SerializableWindows.Editor.Scripts.Settings;
using UnityEditor;

namespace Pindwin.SerializableWindows.Editor.Scripts
{
	public abstract class SerializableWindow<TState> : EditorWindow
		where TState : SerializableState
	{
		protected TState State { get; private set; }

		protected virtual void Awake()
		{
			Initialize();
		}

		protected void Initialize()
		{
			State = SerializableState.Create<TState>();
			EditorUtility.SetDirty(State);
			Reload();
		}

		protected virtual void OnDestroy()
		{
			SaveState();
		}

		protected virtual void Reload()
		{ }

		protected void SaveState()
		{
			string path = $"{SerializableStateSettings.SaveDirectory}/{typeof(TState).Name}.asset";
			TState stateToSave = Instantiate(State);
			AssetDatabase.DeleteAsset(path);
			AssetDatabase.CreateAsset(stateToSave, path);
		}
	}
}
