﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Pindwin.SerializableWindows.Editor.Scripts.Settings;
using UnityEditor;
using UnityEngine;

namespace Pindwin.SerializableWindows.Editor.Scripts
{
	public abstract class SerializableState : ScriptableObject
	{
		public static TState Create<TState>()
			where TState : SerializableState
		{
			string classname = typeof(TState).Name;
			string[] assets = AssetDatabase.FindAssets($"{classname} t:{classname}");
			if (assets != null && assets.Length != 0)
			{
				if (assets.Length > 1)
				{
					LogExcessiveStates(classname, assets);
				}

				return AssetDatabase.LoadAssetAtPath<TState>(AssetDatabase.GUIDToAssetPath(assets[0]));
			}

			EnsureDirectoryExists(SerializableStateSettings.SaveDirectory);
			return CreateStateFile<TState>(SerializableStateSettings.SaveDirectory);
		}

		internal static void MoveAllToLocation(string newLocation)
		{
			string[] assets = AssetDatabase.FindAssets($"t:{nameof(SerializableState)}");
			foreach (string guid in assets)
			{
				string fullPath = $"{newLocation}/{AssetDatabase.GUIDToAssetPath(guid).Split('/').Last()}";
				AssetDatabase.MoveAsset(AssetDatabase.GUIDToAssetPath(guid), fullPath);
			}
		}

		internal static TState CreateStateFile<TState>(string location)
			where TState : SerializableState
		{
			var result = CreateInstance<TState>();
			AssetDatabase.CreateAsset(
				result,
				$"{location}/{typeof(TState).Name}.asset");

			return result;
		}

		internal static void EnsureDirectoryExists(string path)
		{
			string[] folders = path.Split('/');
			if (folders.Length == 0)
			{
				return;
			}

			int i = -1;
			var assetsEncountered = false;
			var sb = new StringBuilder();
			while (++i < folders.Length)
			{
				sb.Append(folders[i]);
				if (assetsEncountered)
				{
					if (!Directory.Exists(sb.ToString()))
					{
						Directory.CreateDirectory(sb.ToString());
					}
				}
				else if (folders[i] == "Assets")
				{
					assetsEncountered = true;
				}

				sb.Append("/");
			}
		}

		private static void LogExcessiveStates(string classname, IEnumerable<string> assets)
		{
			var sb = new StringBuilder();
			sb.AppendLine(
				$"Multiple states detected for SerializableWindow: {classname}. Please close the window and remove unnecessary ones:");
			foreach (string guid in assets)
			{
				sb.AppendLine(AssetDatabase.GUIDToAssetPath(guid));
			}

			Debug.LogWarning(sb.ToString());
		}
	}
}
