﻿using System;
using UnityEditor;
using UnityEngine;

namespace Pindwin.SerializableWindows.Editor.Scripts.Controls
{
	public class DirectoryPicker
	{
		private const string Assets = "Assets";
		private readonly string _absoluteDataPath;

		private readonly string _dataPath;
		private readonly string _label;
		private string _directory;
		private string _directoryName;
		private string _directoryRoot;

		public DirectoryPicker(string directory, string label)
		{
			_dataPath = Application.dataPath.Remove(
				Application.dataPath.Length - Assets.Length,
				Assets.Length);

			Directory = directory;
			_label = label;

			EditorApplication.projectChanged += CheckPathValidity;
		}

		public string Directory
		{
			get { return _directory; }
			private set
			{
				if (!System.IO.Directory.Exists($"{_dataPath}/{value}"))
				{
					value = Assets;
				}

				if (_directory == value)
				{
					return;
				}

				_directory = value;

				RefreshCompounds();

				DirectoryChanged?.Invoke(this, EventArgs.Empty);
			}
		}

		public event EventHandler DirectoryChanged;

		public void Draw()
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField($"{_label} {Directory}");
			if (GUILayout.Button("...", GUILayout.ExpandWidth(false)))
			{
				CheckPathValidity();
				string result = EditorUtility.OpenFolderPanel("Pick a directory", _directoryRoot, _directoryName);
				if (string.IsNullOrEmpty(result))
				{
					return;
				}

				if (result.Length < _dataPath.Length)
				{
					EditorUtility.DisplayDialog(
						"Directory outside project!",
						"Select a folder within Assets directory.",
						"OK");
					return;
				}

				Directory = result.Remove(0, _dataPath.Length);
				CheckPathValidity();
			}

			EditorGUILayout.EndHorizontal();
		}

		private void CheckPathValidity()
		{
			if (!System.IO.Directory.Exists($"{_dataPath}/{Directory}"))
			{
				Directory = Assets;
			}
		}

		private void RefreshCompounds()
		{
			string[] parts = Directory.Split('/');
			if (parts.Length <= 1)
			{
				_directoryName = Directory;
				_directoryRoot = "";
				return;
			}

			_directoryName = parts[parts.Length - 1];
			_directoryRoot = string.Join("/", parts, 0, parts.Length - 1);
		}

		~DirectoryPicker()
		{
			EditorApplication.projectChanged -= CheckPathValidity;
		}
	}
}
