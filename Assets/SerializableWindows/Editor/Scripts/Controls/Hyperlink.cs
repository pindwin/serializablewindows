﻿using UnityEditor;
using UnityEngine;

namespace Pindwin.SerializableWindows.Editor.Scripts.Controls
{
	public class Hyperlink
	{
		private readonly string _label;
		private readonly string _target;

		public Hyperlink(string label, string target)
		{
			_label = label;
			_target = target;
		}

		public void Draw()
		{
			Color c = GUI.color;
			GUI.color = Color.cyan;
			if (GUILayout.Button(_label, EditorStyles.label))
			{
				Application.OpenURL(_target);
			}
			GUI.color = c;
		}
	}
}
