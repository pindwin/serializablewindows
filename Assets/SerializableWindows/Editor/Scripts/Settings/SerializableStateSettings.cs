﻿using UnityEditor;
using UnityEngine;

namespace Pindwin.SerializableWindows.Editor.Scripts.Settings
{
	public class SerializableStateSettings : SerializableState
	{
		public static readonly string DefaultLocation = "Assets/SerializableWindows/Editor/States";

		[HideInInspector] private string _stateLocation;

		public static string SaveDirectory
		{
			get
			{
				string classname = typeof(SerializableStateSettings).Name;
				string[] assets = AssetDatabase.FindAssets($"{classname} t:{classname}");
				if (assets != null && assets.Length != 0)
				{
					return AssetDatabase
						.LoadAssetAtPath<SerializableStateSettings>(AssetDatabase.GUIDToAssetPath(assets[0]))
						.StateLocation;
				}

				EnsureDirectoryExists(DefaultLocation);
				CreateStateFile<SerializableStateSettings>(DefaultLocation);
				return DefaultLocation;
			}
		}

		public string StateLocation
		{
			get { return _stateLocation ?? DefaultLocation; }
			set
			{
				if (string.IsNullOrWhiteSpace(value))
				{
					value = DefaultLocation;
				}

				_stateLocation = value;
			}
		}
	}
}
