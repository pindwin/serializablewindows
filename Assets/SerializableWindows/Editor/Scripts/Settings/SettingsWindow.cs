﻿using System;
using Pindwin.SerializableWindows.Editor.Scripts.Controls;
using UnityEditor;
using UnityEngine;

namespace Pindwin.SerializableWindows.Editor.Scripts.Settings
{
	public class SettingsWindow : SerializableWindow<SerializableStateSettings>
	{
		private DirectoryPicker _picker;
		private Hyperlink _documentationLink;
		private Hyperlink _assetLink;

		protected override void Reload()
		{
			_picker = new DirectoryPicker(State.StateLocation, "Save location:");
			_documentationLink = new Hyperlink("-> Documentation & sourcecode", "https://bitbucket.org/pindwin/serializablewindows/src");
			_assetLink = new Hyperlink("-> Author", "https://www.linkedin.com/in/pindwin/?locale=en_US");
		}

		[MenuItem("Window/Serializable Windows/Settings")]
		private static void CreateWindow()
		{
			var window = GetWindow<SettingsWindow>(false, "Serializable Windows Settings");
			window.Initialize();
			window.Show();
		}

		private void OnGUI()
		{
			_picker.DirectoryChanged -= OnSaveDirectoryChanged;
			EditorGUILayout.LabelField("Serializable Windows Settings", EditorStyles.boldLabel);
			_documentationLink.Draw();
			_assetLink.Draw();
			EditorGUILayout.Space();
			_picker.Draw();
			State.StateLocation = _picker.Directory;
			if (GUILayout.Button("Move all settings to save location", GUILayout.ExpandWidth(false)))
			{
				SerializableState.MoveAllToLocation(State.StateLocation);
			}

			_picker.DirectoryChanged += OnSaveDirectoryChanged;
		}

		private void OnSaveDirectoryChanged(object sender, EventArgs e)
		{
			Repaint();
		}
	}
}
